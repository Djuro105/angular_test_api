import { Component } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {



  constructor(private http: HttpClient) { }
  
  ngOnInit() {
    

    this.fetchData()
  }

/*
  fetchData(){
    
    
    this.http.get("https://eun1.api.riotgames.com/lol/match/v4/matchlists/by-account/0XVsNo8ge9rGx0VSXSL5TPTPUtBEOKDjBg-XsGXzvbQDWQ?api_key=RGAPI-48d8b0c9-c20c-4eef-8f57-534727dcd21d")
    .subscribe(data => {
      console.log(data)
    });
  }
*/
/* CROS PROBLEM PRIMJER
  fetchData(){
    this.http.get("https://eun1.api.riotgames.com/lol/match/v4/matchlists/by-account/0XVsNo8ge9rGx0VSXSL5TPTPUtBEOKDjBg-XsGXzvbQDWQ?api_key=RGAPI-48d8b0c9-c20c-4eef-8f57-534727dcd21d")
    .subscribe(data => {
      console.log(data)
    });
  }
  */


  /////////////////////////////PROCITATI OVO //////////////////////////////////
  // /games je napravljen u proxyconfig.json fajlu
  // link kako se proxi radi   https://www.youtube.com/watch?time_continue=53&v=OjmZPPKaj6A&feature=emb_logo
  // ovaj get api u potpunosti bi glasio  https://eun1.api.riotgames.com/lol/match/v4/matchlists/by-account/0XVsNo8ge9rGx0VSXSL5TPTPUtBEOKDjBg-XsGXzvbQDWQ?api_key=RGAPI-48d8b0c9-c20c-4eef-8f57-534727dcd21d
  // probaj ga u URL ukucat i vidit ces da daje json podatke ali ne znam kako da to dobijem
  // ovdje je link riot api dokumentacije tog odredenog apija  https://developer.riotgames.com/apis#match-v4/GET_getMatch
  fetchData(){
    this.http.get("/games/lol/match/v4/matchlists/by-account0XVsNo8ge9rGx0VSXSL5TPTPUtBEOKDjBg-XsGXzvbQDWQ?api_key=RGAPI-48d8b0c9-c20c-4eef-8f57-534727dcd21d").subscribe(data => {
      console.log(data)
    });
  }
  
  

}
